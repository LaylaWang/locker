import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AssistantTest {

    private Storage storage;

    @BeforeEach
    void setUp() {
        storage = mock(Storage.class);
    }

    @Test
    void should_get_ticket_when_give_bag_to_assistant() {
        Bag bag = new Bag();
        Ticket expect = new Ticket();
        when(storage.save(bag)).thenReturn(expect);

        Assistant assistant = new Assistant(storage);
        Ticket actual = assistant.save(bag);

        assertEquals(expect, actual);
    }

    @Test
    void should_get_bag_when_give_ticket_to_assistant() {
        Ticket ticket = new Ticket();
        Bag bag = new Bag();
        when(storage.get(ticket)).thenReturn(bag);

        Assistant assistant = new Assistant(storage);
        Bag myBag = assistant.get(ticket);

        assertEquals(bag, myBag);
    }

    @Test
    void should_get_message_when_storage_is_out_of_capacity() {
        Bag bag = new Bag();
        when(storage.save(bag)).thenThrow(new RuntimeException("Out of capacity!"));

        Assistant assistant = new Assistant(storage);
        RuntimeException e = assertThrows(RuntimeException.class, () -> assistant.save(bag));
        assertEquals("Out of capacity!", e.getMessage());
    }
}
