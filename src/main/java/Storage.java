import java.util.HashMap;
import java.util.Map;

public class Storage {

    private Map<Ticket, Bag> bags;
    private int size;

    public Storage(int size) {
        this.size = size;
        this.bags = new HashMap<>();
    }

    public Ticket save(Bag bag) {
        if (this.bags.size() >= this.size) {
            throw new RuntimeException("Out of capacity!");
        }
        Ticket ticket = new Ticket();
        this.bags.put(ticket, bag);
        return ticket;
    }

    public Bag get(Ticket ticket) {
        return this.bags.remove(ticket);
    }
}
